### How to run?

* docker-compose build
* docker-compose up

<p>This service is exposed in 7001 port</p>
<p>To open the swagger access the follow address in your browser: http://localhost:7001/swagger-ui.html</p>
