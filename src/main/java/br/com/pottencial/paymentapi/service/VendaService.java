package br.com.pottencial.paymentapi.service;

import br.com.pottencial.paymentapi.model.Produto;
import br.com.pottencial.paymentapi.model.Venda;
import br.com.pottencial.paymentapi.model.Vendedor;
import br.com.pottencial.paymentapi.repository.ProdutoRepository;
import br.com.pottencial.paymentapi.repository.VendaRepository;
import br.com.pottencial.paymentapi.repository.VendedorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class VendaService {

    private final String PAGAMENTO_APROVADO = "Pagamento Aprovado";
    private final String AGUARDANDO_PAGAMENTO = "Pagamento Aprovado";
    private final String CANCELADO = "Pagamento Aprovado";
    private final String ENTREGUE = "Pagamento Aprovado";
    private final String ENVIADO = "Enviado para Transportadora";

    private final VendaRepository vendaRepository;
    private final VendedorRepository vendedorRepository;
    private final ProdutoRepository produtoRepository;

    public Optional<Venda> buscaVendaPorId(Long idVenda) {
        return vendaRepository.findById(idVenda);
    }

    public List<Venda> buscarTodasVendas() {
        return vendaRepository.findAll();
    }

    public boolean salvarVenda(Venda venda) {
        venda.setStatusVenda(AGUARDANDO_PAGAMENTO);
        Optional<Vendedor> vendedorOptional = vendedorRepository.findById(venda.getIdVendedor());
        List<Produto> listaProdutos = produtoRepository.findAllById(venda.getIdProdutos());
        if (vendedorOptional.isPresent() && listaProdutos.size() == venda.getIdProdutos().size()) {
            return vendaRepository.save(venda).getIdVenda() != null;
        }
        return false;
    }

    public boolean atualizarStatusVenda(Long idVenda, String statusVenda) {
        Optional<Venda> vendaOptional = vendaRepository.findById(idVenda);
        if (vendaOptional.isPresent()) {
            String statusAtual = vendaOptional.get().getStatusVenda();
            if (validaStatusVenda(statusAtual, statusVenda)) {
                Venda vendaAtualizada = vendaOptional.get();
                vendaAtualizada.setStatusVenda(statusVenda);
                vendaRepository.save(vendaAtualizada);
                return true;
            }
        }
        return false;
    }

    private boolean validaStatusVenda(String statusAtual, String statusMigracao) {
        HashMap<String, List<String>> transicaoStatus = constroiStatusMigracao();
        return transicaoStatus.get(statusAtual).contains(statusMigracao);
    }

    private HashMap<String, List<String>> constroiStatusMigracao() {
        HashMap<String, List<String>> transicaoStatus = new LinkedHashMap<>();
        transicaoStatus.put(AGUARDANDO_PAGAMENTO, List.of(PAGAMENTO_APROVADO, CANCELADO));
        transicaoStatus.put(PAGAMENTO_APROVADO, List.of(ENVIADO, CANCELADO));
        transicaoStatus.put(ENVIADO, List.of(ENTREGUE));
        return transicaoStatus;
    }
}
