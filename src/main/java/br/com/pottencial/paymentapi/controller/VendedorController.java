package br.com.pottencial.paymentapi.controller;

import br.com.pottencial.paymentapi.model.Vendedor;
import br.com.pottencial.paymentapi.service.VendedorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("vendedor")
public class VendedorController {
    private final VendedorService vendedorService;

    @GetMapping
    public ResponseEntity buscaVendedores() {
        return ResponseEntity.status(HttpStatus.OK).body(vendedorService.buscaVendedores());
    }

    @GetMapping("{id}")
    public ResponseEntity buscaVendedorPorId(@PathVariable("id") Long idVendedor) {
        Optional<Vendedor> vendedorOptional = vendedorService.buscaVendedorPorId(idVendedor);
        if (vendedorOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(vendedorOptional.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @PostMapping
    public ResponseEntity salvaVendedor(@RequestBody Vendedor vendedor) {
        if (vendedorService.salvaVendedor(vendedor)) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @PutMapping("{id}")
    public ResponseEntity atualizaVendedor(@PathVariable("id") Long idVendedor, @RequestBody Vendedor vendedor) {
        if (vendedorService.atualizaVendedor(idVendedor, vendedor)) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).build();
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}
