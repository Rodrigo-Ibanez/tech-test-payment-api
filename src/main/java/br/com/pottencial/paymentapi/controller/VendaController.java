package br.com.pottencial.paymentapi.controller;

import br.com.pottencial.paymentapi.model.Venda;
import br.com.pottencial.paymentapi.service.VendaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("venda")
public class VendaController {
    private final VendaService vendaService;

    @GetMapping
    public ResponseEntity buscaVendas() {
        return ResponseEntity.status(HttpStatus.OK).body(vendaService.buscarTodasVendas());
    }

    @GetMapping("{id}")
    public ResponseEntity buscaVendaPorId(@PathVariable("id") Long idVenda) {
        Optional<Venda> vendaOptional = vendaService.buscaVendaPorId(idVenda);
        if (vendaOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(vendaOptional.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @PostMapping
    public ResponseEntity registraVenda(@RequestBody Venda venda) {
        if (vendaService.salvarVenda(venda)) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @PutMapping("{id}")
    public ResponseEntity atualizaStatusvenda(@PathVariable("id") Long idVenda, @RequestBody String statusVenda) {
        if (vendaService.atualizarStatusVenda(idVenda, statusVenda)) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).build();
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}
